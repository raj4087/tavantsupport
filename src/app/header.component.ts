import { Component } from '@angular/core';
import { ClientDto } from './clientDto.component';
import { InfraDto } from './InfraDto.component';
import { BaseForm } from './form.component';

@Component({
    selector:'header',
    templateUrl: './header.component.html'
  
    
})
export class Header { 
    
    
    constructor(private baseForm:BaseForm){
    }
    onChangeInfra(selectedInfraId) {
    this.baseForm.selectedInfraId=selectedInfraId;
     console.log("selectedClientId:::"+selectedInfraId);
    }

    onChangeClient(selectedClientId) {
    this.baseForm.selectClientId=selectedClientId;
    console.log("selectedClientId:::"+selectedClientId);
    }


}