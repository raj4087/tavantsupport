import { Component,Input  } from '@angular/core';

import {ItemService} from './itemMigartionService.component'
import {BaseForm} from './form.component'
@Component({
    selector:'migrationForm',
     templateUrl: './item.component.html'
})
export class PhotoAssetMigartion{

constructor(private itemService:ItemService,private baseForm:BaseForm){

}
migrate(itemMigartionObj: PhotoAssetMigartion): void {
   this.itemService.StartPhotoAssetMigration(itemMigartionObj);
}
}