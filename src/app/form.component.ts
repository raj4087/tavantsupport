
import {InfraDto} from './InfraDto.component';
import {ClientDto} from './clientDto.component';

export class BaseForm{
    infraIds=[new InfraDto('web1','Web1'),new InfraDto('aws','Aws')];
    clientIds=[new ClientDto('arena','Arena'),new ClientDto('usab','Usab'), new ClientDto('wwe','WWE'),    new ClientDto('eurosport','Eurosport')];
    selectClientId:String;
    selectedInfraId:string;
    isIdgenSame:boolean;
    sourceEnv:String;
    tarrgetEnv:String;
    migrationByItemType:boolean;
    itemId:String;
}